FROM node:14.15
WORKDIR /root/
COPY /src ./src
COPY package*.json ./
RUN npm install

COPY bin/www ./bin/www
COPY gulpfile.js .

EXPOSE 56572

CMD ["npm", "start"]